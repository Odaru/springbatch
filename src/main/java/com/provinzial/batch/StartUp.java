package com.provinzial.batch;

import java.util.Arrays;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * Spring Boot Application wird gestartet. Alle Beans Batch betreffend werden aufgrund der @EnableBatchProcessing Annotation
 * geladen oder da diese in den Projekt mit @Bean annotiert sind. Nach dem Start der Anwendung sind die Jobs im AppContext
 * verfügbar und können via Rest, siehe RestController, angesprochen werden. Durch den propertie Eintrag
 * spring.batch.job.enabled=false wird die Ausführung der Jobs beim Hochfahren des Context unterdrückt.
 */
@SpringBootApplication
@EnableBatchProcessing
public class StartUp {

    /**
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(StartUp.class, args);
    }

    @Bean
    public String getBeans(ApplicationContext ctx) {
        String[] beanDefinitionNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanDefinitionNames);
        for (String beanName : beanDefinitionNames) {
            System.out.println(beanName);
        }
        return "";
    }
}
