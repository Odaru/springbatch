package com.provinzial.batch.job1.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.provinzial.batch.job1.chunks.ItemReaderJob1;
import com.provinzial.batch.job1.chunks.ItemWriterJob1;

@Configuration
@ComponentScan
// @Import(Configurator.class)
public class Job1Config {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private ItemReaderJob1 itemReader;

    @Autowired
    private ItemWriterJob1 itemWriter1;

    @Bean
    public Step demoStep() {
        return stepBuilderFactory.get("step1").<String, String> chunk(1).reader(itemReader).writer(itemWriter1).build();
    }

    @Bean
    public Job testJob1() {
        return jobBuilderFactory.get("job1").start(demoStep()).build();
    }

}
