package com.provinzial.batch.job1.chunks;

import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.stereotype.Component;

/**
 * @author pd05613
 */
@Component
public class ItemReaderJob1 implements org.springframework.batch.item.ItemReader<String> {

    static String[] input = {"A", "B"};

    static int index = 0;

    /*
     * (non-Javadoc)
     * @see org.springframework.batch.item.ItemReader#read()
     */
    @Override
    public String read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

        if (index < input.length) {
            String temp = input[index];
            index++;
            return temp;
        }
        return null;
    }

}
