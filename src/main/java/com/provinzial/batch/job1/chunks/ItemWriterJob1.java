package com.provinzial.batch.job1.chunks;

import java.util.List;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

/**
 * @author pd05613
 */
@Component
public class ItemWriterJob1 implements ItemWriter<String> {

    /*
     * (non-Javadoc)
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(List<? extends String> items) throws Exception {
        for (String string : items) {
            System.out.println("Writer: " + string);
        }

    }

}
