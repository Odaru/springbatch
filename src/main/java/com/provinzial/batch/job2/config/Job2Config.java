package com.provinzial.batch.job2.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.PassThroughLineMapper;
import org.springframework.batch.item.file.transform.PassThroughLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

@Configuration
@ComponentScan
// @Import(Configurator.class)
public class Job2Config {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Step demoStep() {
        return stepBuilderFactory.get("step1").<String, String> chunk(1).reader(createFlatFileItemReader())
            .writer(createFlatFileItemWriter()).build();
    }

    @Bean
    public Job testJob2() {
        return jobBuilderFactory.get("job2").start(demoStep()).build();
    }

    private FlatFileItemReader<String> createFlatFileItemReader() {
        FlatFileItemReader<String> reader = new FlatFileItemReader<String>();

        reader.setLineMapper(getLineMapper());
        reader.setResource(
            new FileSystemResource("C:\\Entwicklung\\projekte\\SpringBatch\\SpringBatch\\src\\main\\resources\\InputJob2.txt"));

        return reader;
    }

    private LineMapper<String> getLineMapper() {
        return new PassThroughLineMapper();
    }

    private FlatFileItemWriter<String> createFlatFileItemWriter() {
        FlatFileItemWriter<String> writer = new FlatFileItemWriter<String>();

        writer.setEncoding("UTF-8");
        writer.setResource(new FileSystemResource(
            "C:\\Entwicklung\\projekte\\SpringBatch\\SpringBatch\\src\\main\\resources\\OutputJob2.txt"));
        writer.setLineAggregator(new PassThroughLineAggregator<>());
        return writer;
    }

}
