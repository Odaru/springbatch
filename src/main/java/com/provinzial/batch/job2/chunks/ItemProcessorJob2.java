package com.provinzial.batch.job2.chunks;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

/**
 * @author pd05613
 */
@Component
public class ItemProcessorJob2 implements ItemProcessor<String, String> {

    /*
     * (non-Javadoc)
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public String process(String item) throws Exception {
        return item;
    }

}
