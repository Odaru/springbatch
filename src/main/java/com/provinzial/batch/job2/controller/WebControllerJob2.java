package com.provinzial.batch.job2.controller;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Diese Klasse stellt die Schnittstelle bereit, um einen Batch Job zu starten. Es wird eine URL festgelegt welche beim Aufruf
 * dann den Job anstößt.
 */
@RestController
public class WebControllerJob2 implements ApplicationContextAware {

    // JobLauncher befindet sich standardmäßig im ApplicationContext und muss nur Autowired werden
    @Autowired
    private JobLauncher launcher;

    private ApplicationContext applicationContext;

    @GetMapping("/startJob2")
    public void startJob2() {
        long start = System.currentTimeMillis();

        try {
            JobParameters jobParameters =
                new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();

            launcher.run((Job) applicationContext.getBean("testJob2"), jobParameters);
        } catch (Exception e) {
            e.printStackTrace();
        }

        long completion = System.currentTimeMillis() - start;

    }

    /*
     * (non-Javadoc)
     * @see
     * org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

}
